import time
import json
import logging
import os
from lib.octopyapp.octopyapp import OctopyApp
import argparse


APP_ID = 'Storage Manager'
LOG_LEVEL = logging.DEBUG

DEFAULT_CATEGORY = 'uncategorized'


class StorageManager(OctopyApp):
    def __init__(self, config):
        super().__init__(APP_ID, LOG_LEVEL, config)
        self.subscribe(self.topics['storage']['set'], self.on_set_message)
        self.subscribe(self.topics['storage']['get'], self.on_get_message)
        self.subscribe(self.topics['storage']['mget'], self.on_multiple_get_message)
        self.subscribe(self.topics['storage']['delete'], self.on_delete_message)


    def verify_config(self):
        super().verify_config()
        if not ('storage' in self.config) or not ('path' in self.config['storage']):
            raise ConfigurationError('Key "storage":"path" is missing in configuration')
        if not self.config['storage']['path'] or self.config['storage']['path'] == '':
            raise ConfigurationError('Value for "storage":"path" is missing in configuration')


    def get_value(self, key, category):
        path = os.path.join(self.config['storage']['path'], category)
        filepath = os.path.join(path, key)
        if not os.path.exists(filepath):
            raise KeyError(f"{category}:{key} does not exist")
        logging.debug(f"Reading \"{category}:{key}\" from file \"{filepath}\"")
        f_in = open(filepath, "r")
        blob = f_in.read()
        f_in.close()
        try:
            data = json.loads(blob)
        except json.decoder.JSONDecodeError as err:
            raise ValueError(f"JSON encoding error for value \"{category}:{key}\" in file \"{filepath}\: {err}")
        logging.debug(f"\"{category}:{key}\" retreived from \"{filepath}\"")
        return data


    def on_set_message(self, client, userdata, msg):
        try:
            payload = json.loads(msg.payload)
            if not 'key' in payload:
                logging.warning("Set failed: no key given")
                return
            key = payload['key']
            category = payload['category'] if 'category' in payload else DEFAULT_CATEGORY
            value = payload.get('value')
            path = os.path.join(self.config['storage']['path'], category)
            os.makedirs(path, exist_ok=True)
            filepath = os.path.join(path, key)
            f_out = open(filepath, "w")
            data = {
                'value': value,
                'timestamp': time.time()
            }
            blob = json.dumps(data, indent = 4)
            logging.debug(f"Writing \"{category}:{key}\" to file: \"{filepath}\"")
            f_out.write(blob)
            f_out.close()
            logging.debug(f"\"{category}:{key}\" stored in \"{filepath}\"")
        except json.decoder.JSONDecodeError as err:
            logging.warning(f"JSON parse error for set message (\"{msg.topic}\"): {err}")
        except Exception as err:
            logging.warning(f"Error setting value: {err}")


    def on_get_message(self, client, userdata, msg):
        return_payload = {
            "command": "get"
        }
        err_msg = None
        try:
            payload = json.loads(msg.payload)
            if not 'key' in payload:
                logging.warning("Get failed: no key given")
                return
            key = payload['key']
            category = payload['category'] if 'category' in payload else DEFAULT_CATEGORY
            return_topic = self.topics['storage']['default return']
            if 'return topic' in payload:
                return_topic = payload['return topic']
            value = self.get_value(key, category)
            return_payload = value
            logging.debug(f"Returning value to '{return_topic}'")
        except json.decoder.JSONDecodeError as err:
            err_msg = f"JSON parse error for get message: {err}"
            logging.error(f"JSON parse error for get message (\"{msg.topic}\"): {err}")
            return
        except Exception as e:
            if category and key:
                err_msg = f"Could not read value '{category}:{key}': {e}"
            else:
                err_msg = f"Could not read value: {e}"
            logging.error(err_msg)
            return
        if 'key' in payload:
            return_payload['key'] = payload['key']
        if 'category' in payload:
            return_payload['category'] = payload['category']
        if 'call id' in payload:
            return_payload['call id'] = payload['call id']
        if err_msg:
            return_payload['error'] = err_msg
        # Return value or error message
        self.publish(topic = return_topic, payload = json.dumps(return_payload))


    def on_multiple_get_message(self, client, userdata, msg):
        return_payload = {
            "command": "mget"
        }
        err_msg = None
        try:
            payload = json.loads(msg.payload)
            if not 'keys' in payload:
                logging.warning("Get failed: no keys given")
                return
            keys = payload['key']
            category = payload['category'] if 'category' in payload else DEFAULT_CATEGORY
            return_topic = self.topics['storage']['default return']
            if 'return topic' in payload:
                return_topic = payload['return topic']
            values = []
            for key in keys:
                try:
                    value = self.get_value(key, category)
                    if value:
                        values['key'] = key
                        values['category'] = category
                        values.append(value)
                    else:
                        logging.warning(f"Error getting value '{key}': {err}")
                except Exception as err:
                    logging.warning(f"Error getting value '{key}': {err}")
            logging.debug(f"Returning value to '{return_topic}'")
        except json.decoder.JSONDecodeError as err:
            err_msg = f"JSON parse error for mget message: {err}"
            logging.error(f"JSON parse error for mget message (\"{msg.topic}\"): {err}")
        except Exception as err:
            if category and keys:
                err_msg = f"Could not read multiple values '{category}:{keys}': {err}"
            else:
                err_msg = f"Could not read multple values: {err}"
            logging.error(err_msg)
        if 'keys' in payload:
            return_payload['keys'] = payload['keys']
        if 'category' in payload:
            return_payload['category'] = payload['category']
        if 'call id' in payload:
            return_payload['call id'] = payload['call id']
        if err_msg:
            return_payload['error'] = err_msg
        # Return value or error message
        self.publish(topic = return_topic, payload = json.dumps(return_payload))


    def on_delete_message(self, client, userdata, msg):
        try:
            payload = json.loads(msg.payload)
            if not 'key' in payload:
                logging.warning("Delete failed: no key given")
                return
            category = payload['category'] if 'category' in payload else 'uncategorized'
            key = payload['key']
            path = os.path.join(self.config['storage']['path'], category)
            filepath = os.path.join(path, key)
            value = None
            if not os.path.exists(filepath):
                logging.warning(f"{payload['category']}:{key} does not exist")
            else:
                os.remove(filepath)
                logging.debug(f"\"{key}\" deleted from \"{filepath}\"")
        except json.decoder.JSONDecodeError as err:
            err_msg = f"JSON parse error for delete message: {err}"
            logging.error(f"JSON parse error for delete message (\"{msg.topic}\"): {err}")
        except Exception as e:
            logging.error(f"Error deleting value: {e}")



if __name__ == '__main__':
    # Parse command line arguments
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--path',               help="Path to data storage", type=str, default="../config-db")
    arg_parser.add_argument('--host',               help="MQTT host/IP", type=str, default="")
    arg_parser.add_argument('--port',               help="MQTT port", type=int, default=None)
    arg_parser.add_argument('--username', '--user', help="MQTT username", type=str, default="")
    arg_parser.add_argument('--password', '--pass', help="MQTT password", type=str, default="")
    arg_parser.add_argument('--prefix',             help="MQTT prefix", type=str, default="")
    args = arg_parser.parse_args()
    # Setup config object from arguments.
    # The structure matches the structure expected of the configuration files
    config = {
        'mqtt': {}
    }
    if args.host:
        config['mqtt']['host'] = args.host
    if args.port:
        config['mqtt']['port'] = args.port
    if args.username:
        config['mqtt']['username'] = args.username
    if args.password:
        config['mqtt']['password'] = args.password
    if args.prefix:
        config['mqtt']['prefix'] = args.prefix
    if args.path:
        config['storage'] = {
            'path': args.path
        }
    # Create and start the Storage Manager object
    sm = StorageManager(config)
    sm.start()
