# Storage Manager

## Overview
The Storage Manager handles all persistent data storage for Octopy. Any data that
needs to be saved between sessions should be stored using the Storage Manager.


## Installation
Requires Python 3.6

Simply clone the repository and optionally edit the config.json configuration file.

### Required Python packages
* paho-mqtt
* ujson

### Required Octopy modules
None

## Configuration
Configuration is done in the config.json file.
Normally you would only need to change the MQTT server and port settings, and
the data storage path.

Example configuration:

```javascript
{
    "mqtt": {
        "server": "sad-vm-mqtt-001.cslab.esss.lu.se",
        "port": 1883
    },

    "storage": {
        "path": "db/"
    },

    "topics": {
        "set": "octopy/storage/set",
        "get": "octopy/storage/get",
        "delete": "octopy/storage/delete",
        "default return": "octopy/storage/return"
    }
}
```

## Running
Start the program by executing the `start.sh` shell script, alternatively use
`python3 storage-manager.py`



## API
The Storage Manager uses a simple MQTT-based API with data encoded in JSON.
Data is stored according to category and id. The category is used for logical
grouping of the data to avoid too much data in the same place and potential
data clobbering. The id is the name of the data. The cateogry and id combination
uniquely identifies the data.

### Store data
#### MQTT-topic
octopy/storage/set
#### Payload format
```javascript
{
    "category": <category string>,
    "key": <id string>,
    "value": <object or value>
}
```
* category: the category group to store the data under.
* key: the unique id of the data.
* value: a JSON object or value that contains the data to be stored.

#### Response
none


### Retrieve data
#### MQTT-topic
octopy/storage/get
#### Request format
```javascript
{
    "category": <string>,
    "key": <string>,
    "return topic": <string>
}
```
* category: the category group that the data is stored under.
* key: the unique id of the data.
* return_topic [optional]: the MQTT topic that the data should be returned over. If not given the default return topic from the config.json will be used.
#### Response
```javascript
{
    "category": <string>,
    "key": <string>,
    "timestamp": <number>,
    "value": <object or value>
}
```
* category: the category group that the data was fetched from.
* key: the unique id of the data.
* timestamp: Unix timestamp for when the data was stored.
* value: the data.


### Retrieve multiple data
#### MQTT-topic
octopy/storage/mget
#### Request format
```javascript
{
    "category": <string>,
    "keys": [<string>, <string>, <string>, ...],
    "return topic": <string>
}
```
* category: the category group that the data is stored under.
* keys: an array of one or more keys to retreive.
* return_topic [optional]: the MQTT topic that the data should be returned over. If not given the default return topic from the config.json will be used.
#### Response
The response is an array of response objects of the same format as for the single get command
```javascript
[
    {
        "category": <string>,
        "keys": <string>,
        "timestamp": <number>,
        "value": <object or value>
    },
    {
        "category": <string>,
        "keys": <string>,
        "timestamp": <number>,
        "value": <object or value>
    },
    ...
]
```
* category: the category group that the data was fetched from.
* key: the unique id of the data.
* timestamp: Unix timestamp for when the data was stored.
* value: the data.
